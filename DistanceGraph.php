
<html>
    <head>
        <title>Mice 1</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="MiceCSS.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    </head>
    <body>
        <div class="w3-bar w3-black">
            <a href="index.php" class="w3-bar-item w3-button w3-mobile w3-blue">Sign out</a>
            <a href="home.html" class="w3-bar-item w3-button w3-mobile">Home</a>
            <a href="About.html" class="w3-bar-item w3-button w3-mobile">About</a>
            <a href="Predictions.html" class="w3-bar-item w3-button w3-mobile">Predictions</a>
            <div class="w3-dropdown-hover w3-mobile">
            <button class="w3-button">Graphs <i class="fa fa-caret-down"></i></button>
            <div class="w3-dropdown-content w3-bar-block w3-dark-grey">
                <a href="TimeGraphs.html" class="w3-bar-item w3-button w3-mobile">Duration Graphs</a>
                <a href="DistanceGraphs.html" class="w3-bar-item w3-button w3-mobile">Distance Graphs</a>
            </div>
            </div>
        </div>    
        
        
        <script type="text/javascript">
            $(document).ready(function () {
            $('#submit').click(function() {
                checked = $("input[type=checkbox]:checked").length;

            if(!checked) {
                alert("You must select at least one mouse!");
                return false;
            }

            });
            });
        </script>
        <h1>Distance Graphs</h1>
        
        <div id="search">
        <form action="DisplayDistanceGraph.html">
            <h3>Select which mice and start/end time you would like to view on graph:</h3>
            <?php

            $connect = mysqli_connect('localhost', 'root', '', 'dbtest');

            $sql = "SELECT DISTINCT `name` FROM mice";

            $query_resource = mysqli_query($connect,$sql);

            
            $mice = mysqli_fetch_assoc($query_resource)

            ?>
            
            <span><?php echo $mice['name']; ?></span>
            <input type="checkbox" name="mice[]" value="<?php echo $mice['name'] ?> /><br />   
            
            <?php endwhile; ?>"
            

            
            <br><br>
            Start Time:
            <input type="datetime-local" name="start" required><br><br>
            End Time:
            <input type="datetime-local" name="end" required><br><br> 
            <input type="submit" id="submit">
        </form>
        </div>
    </body>
</html>