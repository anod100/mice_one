import psycopg2
import csv
import sys
from datetime import datetime
from datetime import timedelta

MAX_SENSOR = 32

GOOGLE_EPOCH = datetime.strptime('30/12/1899', '%d/%m/%Y')

conn = psycopg2.connect("host=localhost dbname=dbtest user=usertest password=usertest")

cur = conn.cursor()
cur.execute('DROP TABLE if exists "time";')
cur.execute('DROP TABLE if exists "scanner";')
cur.execute('DROP TABLE if exists "mice";')

cur.execute('create table scanner (ID int not null, locX int not null, locY int not null, Primary key(ID));')
cur.execute('create table mice (name text not null, Primary key(name));')
cur.execute('create table time ( Time timestamp, name text references mice(name), scanner int references scanner(ID));')

conn.commit()

with open(sys.argv[1],'r') as f:
	reader = csv.reader(f,delimiter=';')
	#next(reader) #skips the header row
	counter = 0
	for row in reader:
		if counter < MAX_SENSOR:
			#id-device / numberID / locX / locY / 0 / SAM
			#print(row[1])
			counter += 1
			#if int(row[2]) == 0 and int(row[3]) == 0:
				#continue
			cur.execute(
				"INSERT INTO scanner VALUES (%s, %s, %s)",
				(row[1], row[2], row[3])
			)
			#counter += 1
			#conn.commit()
		else:
			#time/ id / name / scanner / timeon /;;; / 1
			#print(row[3])
			#counter+=1
			#print(counter)
			#print( datetime.utcfromtimestamp( float( row[0] ) ) )
			#print( float( row[0] ) )

			#CHECK IF A MOUSE IN THE MICE TABLE AND IF NOT ADD IT;
			if row[2] == 'unkown':
				continue
			#cur.execute("SELECT * from scanner where ID = '{}' ".format(int(row[3])))	
			#if(cur.rowcount == 0):
				#continue
			cur.execute(("select * from mice where name = '{}'").format (row[2].strip()))
			if cur.rowcount == 0:
				cur.execute(("INSERT INTO mice VALUES ('{}')").format (row[2].strip()))
			#populate the time table
			cur.execute((
				"INSERT INTO time VALUES ('{}', '{}', '{}')").format(GOOGLE_EPOCH + timedelta(days=float(row[0])), row[2].strip(), row[3]) 
				#(datetime.utcfromtimestamp(float(row[0])).strftime('%Y-%m-%d %H:%M:%S.%f'), row[1], row[2], row[3])
			)

conn.commit()
print("Database created and populated")

#cur.execute("select * from Time")
#print
#print(cur.fetchone())
#print
#cur.execute("select distinct name from time")
#print(cur.fetchall())

