let express = require("express");
//let Pool = require('pg').Pool;
let bodyParser = require('body-parser');

var http = require('http');
var fs = require('fs');
var path = require('path');

const GraphSetUp = require('./graphsetup.js');
let prox = new GraphSetUp();
let dist = new GraphSetUp();
let time = new GraphSetUp();

const spawn = require("child_process").spawn;
//const pythonProcess = spawn('python', ["path/to/script.py",arg1,arg2, ...]);

const app = express();

const portNumber = 8080;

const FILEPATH = 'mice_one';

const {BigQuery} = require('@google-cloud/bigquery');
const bigquery = new BigQuery();

const scaners = {1:[473,31],2:[473,182],3:[433,107],4:[433,258],5:[393,182],6:[393,182],7:[353,107],8:[353,107],9:[313,31],10:[313,182],11:[273,107],12:[273,258],13:[232,31],14:[232,182],15:[192,107],16:[192,258],17:[152,31],18:[152,182],19:[112,107],20:[112,258],21:[72,31],22:[72,182],23:[32,107],24:[32,258],25:[0,0],26:[0,0],27:[0,0],28:[0,0],29:[0,0],30:[0,0],31:[0,0],32:[0,0]};

/*
   var config = {
host: 'localhost',
user: 'USER_TO_BE_CHANGED',
password: 'PASSWORD_TO_BE_CHANGED',
database: 'SOMETHING_TO_DO_WITH_MICE',
};
*/

var dateFromat = require('dateformat');

//var pool = new Pool(config);

var login = false;

app.set('port',(portNumber));

app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true}));

/*app.get/set/use*/
/*
   app.use(function(req,res,next){
   res.setHeader('Access-Control-Allow-Orgin','*');
   next();
   });
   */
//console.log("running");
app.use(express.static(FILEPATH));

/*	HANDLE REQUESTS				*/

//app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){//the first page
	res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/home.html', function(req,res){//main page
	//console.log(req);
	if(req.query.username === "admin" && req.query.password === "admin"||login){
		login = true;
		res.sendFile(path.join(__dirname + '/home.html'));
	}else{
		res.sendFile(path.join(__dirname + '/index.html'));
	}
});

app.get('/index.html',function(req,res){//logout button
	login = false;
	res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/About.html',function(req,res){
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/About.html'));
});

//navigate to input pages
app.get('/DistanceGraphs.html',function(req,res){
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/DistanceGraphs.html'));
});

app.get('/TimeGraphs.html',function(req,res){  
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/TimeGraphs.html'));  
});  

app.get('/ProxGraphs.html',function(req,res){ 
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/ProxGraphs.html')); 
});  
//mavigate to input pages

//not needed for this project

app.get('/Predictions.html',function(req,res){
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.json({"ERROR": "Not IMPLEMENTED"})
});

//display pages

//distance graph
app.get('/DisplayDistanceGraph.html', async(req, res)=>{
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/DisplayDistanceGraph.html'));  
});

//time graph
app.get('/DisplayGraph.html',async(req, res)=>{//TIME GRAPH
	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/DisplayGraph.html'));  
});

//proximity
app.get('/DisplayProx.html',async(req, res)=>{
	const sqlQuery = `SELECT *
		FROM MiceData.Mice
	WHERE Colony = "OBX"`;
	const options = {
		query: sqlQuery,
		location: 'US',
	};

	const [rows] = await bigquery.query(options);

	//prox.populateGraph(JSON.stringify(rows),"PROX");
	prox.setTitle("Proximity of mice from selected mouse","distance from mouse(units)");

	if(!login){res.sendFile(path.join(__dirname + '/index.html'));}
	res.sendFile(path.join(__dirname + '/DisplayProx.html'));
	//res.json({'fuck':'this'})
});
//display pages


/*	app.get(url,async(req, res)=>{code});	*/


//Test functiona ---
app.get('/hello', async (req, res) => {
	res.json({'response': 'Hello World'});
});


app.get('/testgoogle', async (req, res) => {
	console.log("running");
	// The SQL query to run
	const sqlQuery = `SELECT *
		FROM  MiceData.Mice 
	WHERE Colony = "OBX"
	limit 500`;

	const options = {
		query: sqlQuery,
		//Location must match that of the dataset(s) referenced in the query.
		location: 'US',
	};

	// Runs the query
	//res.json("waiting");
	const [rows] = await bigquery.query(options);


	prox.setTitle("time spent traveling","distance from mouse (units)");
	prox.populateGraph(JSON.stringify(rows),"TIME","GREEN3");

	var data = prox.getData();
	var layout = prox.getLayout();
	//Ploty.newPlot('myDiv',data,layout);

	console.log('Query Results:');

	res.json(rows);
});
//--- Test functions

app.listen(app.get('port'), () => {
	console.log('running on port ' + portNumber);
});
