class GraphSetUp {

	constructor(){
		this.mode = 'lines+markers';
		this.x_ax = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		this.width = 1;
		//var data = new Array();
		//var mice = new Array();
		//var time = new Array();
		this.yax_values = [];
		this.xLabel = 'Hour of Day';
		//var yLabel = '';
		//var title = '';

		this.scaners = {1:[473,31],2:[473,182],3:[433,107],4:[433,258],5:[393,182],6:[393,182],7:[353,107],8:[353,107],9:[313,31],10:[313,182],11:[273,107],12:[273,258],13:[232,31],14:[232,182],15:[192,107],16:[192,258],17:[152,31],18:[152,182],19:[112,107],20:[112,258],21:[72,31],22:[72,182],23:[32,107],24:[32,258],25:[0,0],26:[0,0],27:[0,0],28:[0,0],29:[0,0],30:[0,0],31:[0,0],32:[0,0]}; 
	}


	setTitle(title, yLabel){
		this.title = title;
		this.yLabel = yLabel;
	}

	makeColor(MouseName) {
		var color = '';
		if(MouseName.toUpperCase().includes('RED')){
			color = 'rgb(255,0,0)';
		}else if(MouseName.toUpperCase().includes('BLUE')){
			color = 'rgb(0,0,255)';
		}else if(MouseName.toUpperCase().includes('GREEN')){
			color = 'rgb(0,255,0)';
		}else if(MouseName.toUpperCase().includes('PURPLE')){
			color = 'rgb(175,0,255)';
		}else if(MouseName.toUpperCase().includes('YELLOW')){
			color = 'rgb(0,255,255)';
		}else{
			color = 'rgb(255,255,0)';
		}
		return color;
	}

	getData() {
		return this.data;
	}

	getLayout() {
		var layout = {	title :  this.title , xaxis : {title:  this.xLable}, yaxis : {title: this.yLabel }};
		return layout;
	}

	populateGraph(info, name, selected = null) {
		console.log("setting up graph...");
		this.mice = new Array();
		var yax_values = [];// this will hold the un averaged version of what should be in this.yax_values;
		this.yax_values = [];
		this.time = [];
		this.data = new Array();

		var slect;

		//console.log(info);
		info = JSON.parse(info);
		console.log(info);
		for(var i in info){
			//info.properity(i);
			if (this.mice.indexOf(info[i].MouseName) <= -1){//we need to register the mouse	
				yax_values.push([]);
				this.time.push([]);
				this.yax_values.push([]);
				this.mice.push(info[i].MouseName);
				if(info[i].MouseName == selected){
					slect = this.mice.indexOf(info[i].MouseName);
					console.log("captured the mouse index");
				}
				//console.log("HERE I AM "+info[i].MouseName);
			}
			//console.log("should have a log above me");
			//console.log("asb  "+ info[i].DTS);
			//console.log("oof " + info[i].DTS.value);
			this.time[this.mice.indexOf(info[i].MouseName)].push(new Date(info[i].DTS.value));
			//console.log(this.time[this.mice.indexOf(info[i].MouseName)]+" OBNOXIOUS TEXT");

			switch(name){	//we need differrnt info based on the type of graph
				case "PROX":
					yax_values[this.mice.indexOf(info[i].MouseName)].push(info[i].GridID);
					//get the grid ID of each cycle, will need to be modifyed to convert to distance from particular mouse
					break;
				case "DIST":
					yax_values[this.mice.indexOf(info[i].MouseName)].push(info[i].GridID);
					//get the grid ID of each cycle, will need to find the distance between two elements and keep track of the time
					break;
				case "TIME":
					
					yax_values[this.mice.indexOf(info[i].MouseName)].push(info[i].Duration);
					//console.log(yax_values[this.mice.indexOf(info[i].MouseName)]);
					//get the duration of time the mouse was on the scanner
					break;
				default:
					break;
			}
		}
		

		console.log("yax_values");
		console.log(yax_values);

		if(name == "DIST" || name == "TIME"){
			//now that we have our working data we need to average and calculate the re;vent information
			console.log("we in it bois");
			for(var mouse in yax_values){
				for(var i = 0; i < 24; i++){
					var place_holder = 0;
					var counter = 0;
					for(var value in yax_values[mouse]){
						console.log(typeof(this.time[mouse][value]));
						console.log(this.time[mouse][value]);
						console.log(this.time[mouse][value].getHours());
						if(this.time[mouse][value].getHours() == i){
							switch(name){
								case "DIST":
                                                                    if(value > 0){   
                                                                        // THIS IS WHAT I AM CURRENTLY WORKING ON
                                                                        //---IF HAVEN'T SEEN MOUSE IN ARRAY---
                                                                        //scanners tick every about 200 m/s
                                                                        //mice-time-whatever looking for 
                                                                        //duration = m/s
                                                                        //Order = scanner number/unit x/ unit y
                                                                                                                                                
                                                                    }
									break;
								case "TIME":
									place_holder += yax_values[mouse][value];
									break;
							}
							counter++;

						}
					}
					if(counter == 0){counter++;}
					this.yax_values[mouse].push(place_holder);//we dont need to run an average if we're just counting up time, ONLY WORKS IF THE DAY IS = 1;
					console.log(this.yax_values);
				}
			}//we need 24 y values 0 (midnight to 23:00)
		}else{//name == PROX
			console.log("name==Prox");
			var dist = [[]];
			for(loc in yax_values[slect]){
				for(mouse in yax_values){
					var place_holder;
					if(mouse == slect){
						continue;
					}
					for(oLoc in yax_values[mouse]){
						if(time[mouse][oLoc] >= time[slect][loc]  && oLoc > 0){
							var x = this.scaners[yax_values[mouse][oLoc-1]][0] - this.scaners[yax_values[slect][loc]][0];
							var y = this.scaners[yax_values[mouse][oLoc-1]][1] - this.scaners[yax_values[slect][loc]][1];
							dist[mouse].push(sqrt(x*x+y*y));
							console.log("This is running also here is dist[mouse]"+ dist[mouse]);
						}
					}

				}
			}
			for(mouse in dist){
				for(var i = 0; i < 24; i++){
					var place_holder;
					var counter = 0;
					for(value in dist[mouse]){
						if(time[mouse][value].getHours() == i){
							place_holder += dist[mouse][value];
							counter++;
						}
						
					}
					this.yax_values[mouse].push(place_holder/counter);
				}
			}
		}

		//put all of our information into info
		for(var i in this.mice){
			var trace = {
				x: this.x_ax,  
				y: this.yax_values[i],//y values for mouse i
				mode: this.mode,  
				name: this.mice[i],
				marker: {color: this.makeColor(this.mice[i]),},
				line: {color: this.makeColor(this.mice[i]), width: this.width,}
			};
			this.data.push(trace);
		}
		console.log(this.data);

		return;
	}

};
module.exports = GraphSetUp;
